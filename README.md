---
description: A MultiselectAccordion component.
---

import { MultiselectAccordion } from './multiselect-accordion';

The MultiselectAccordion component can handle multiple selections with checkmarks and grouping subsets with accordion.
you can check uncheck the entire grouping by clicking over the accordion summary title checkmark.

### Component installation

Safe install

```sh
npm i react-multiselect-accordion
```

### Component usage

```js
import { MultiselectAccordion } from 'react-multiselect-accordion';
```

The component requires the props below:

| prop           | description                                            |
| -------------- | ------------------------------------------------------ |
| label          | String that shows the floating label in the component. |
| data           | the data used by the component.                        |
| selectAllLabel | label used to show the select all option and value.    |

an example of the data used is below:

```js
const data = [
  { id: 1, text: 'Finland', grouping: 'Europe' },
  { id: 2, text: 'Germany', grouping: 'Europe' },
  { id: 3, text: 'France', grouping: 'Europe' },
  { id: 4, text: 'Virgin Islands (Brithis)', grouping: 'UK and Eire' },
  { id: 5, text: 'Monserrat', grouping: 'UK and Eire' },
  { id: 6, text: 'United Kingdom', grouping: 'UK and Eire' },
];
```

### Return value

The return value of the component is an array with the selected Ids (numbers).

### Default values

This component has some default values for the below props:

| prop                 | description                                       | value         | type    |
| -------------------- | ------------------------------------------------- | ------------- | ------- |
| size                 | sets the size of the component default to (small) | small, normal | string  |
| fullWidth            | sets the full with                                | true          | boolean |
| selectedAllByDefault | sets all the values selected by default           | true          | boolean |
| skeleton             | shows the skeleton component                      | false         | boolean |
