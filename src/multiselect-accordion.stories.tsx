import * as React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { MultiselectAccordion } from "../dist/index";

const data = [
  { id: 1, text: "Finland", grouping: "Europe" },
  { id: 2, text: "Germany", grouping: "Europe" },
  { id: 3, text: "France", grouping: "Europe" },
  { id: 4, text: "Virgin Islands (Brithis)", grouping: "UK and Eire" },
  { id: 5, text: "Monserrat", grouping: "UK and Eire" },
  { id: 6, text: "United Kingdom", grouping: "UK and Eire" },
];

export default {
  title: "MultiselectAccordion",
  component: MultiselectAccordion,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    data,
  },
} as ComponentMeta<typeof MultiselectAccordion>;

const Template: ComponentStory<typeof MultiselectAccordion> = (args) => (
  <MultiselectAccordion {...args} />
);

export const Default = Template.bind({});

Default.args = {
  data
};
