const { MultiselectAccordion } = require('../dist');
const {render} = require('@testing-library/react');

test('should show multiselect accordion', () => {
  // Arrange
  const { queryByLabelText } = render(<MultiselectAccordion label="Territories" data={[{ id: 1, name: 'Finland', grouping: 'Europe' }]} />)

  // Act
  

  // Assert
  expect(queryByLabelText(/territories/i)).toBeTruthy();
})


