import Checkbox from '@mui/material/Checkbox';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select from '@mui/material/Select';
import Skeleton from '@mui/material/Skeleton';

import { SelectAccordionItem } from './multiselect-accordion-item';
import { useMultiselectValues } from './multiselect-accordion.hooks';
import { MenuProps } from './theme-tokens';
import { MultiselectAccordionProps } from './types';

export function MultiselectAccordion({
  label,
  selectedAllByDefault = true,
  selectAllLabel,
  data,
  fullWidth = true,
  size = 'small',
  sx,
  name,
  skeleton = false,
  onChange = () => {},
  value: defaultValue,
  ...rest
}: MultiselectAccordionProps) {
  const { names, setNames, getValue, getRenderValue, groups, selectAllToggle } =
    useMultiselectValues({
      data,
      selectAllLabel,
      onChange,
      selectedAllByDefault,
      defaultValue,
    });

  return (
    <FormControl fullWidth={fullWidth} size={size}>
      {skeleton ? (
        <Skeleton sx={{ height: `${size === 'small' ? '37' : '47'}px` }} />
      ) : (
        <>
          <InputLabel id={`multiple-checkbox-label-${name}`}>
            {label}
          </InputLabel>
          <Select
            name={name}
            labelId={`multiple-checkbox-label-${name}`}
            id="multiple-checkbox"
            multiple
            value={getValue()}
            input={<OutlinedInput label={label} />}
            renderValue={getRenderValue}
            MenuProps={MenuProps}
            fullWidth={fullWidth}
            sx={{ ...sx }}
            {...rest}
          >
            {selectAllLabel && (
              <MenuItem onClick={selectAllToggle}>
                <Checkbox
                  checked={names.length === data.length}
                  inputProps={{ 'aria-label': 'controlled' }}
                />
                <ListItemText primary={selectAllLabel} />
              </MenuItem>
            )}

            {groups.map(({ data, grouping }) => (
              <SelectAccordionItem
                key={grouping}
                data={data}
                names={names}
                setName={setNames}
                territoryName={grouping}
              />
            ))}
          </Select>
        </>
      )}
    </FormControl>
  );
}
