import * as React from 'react';
import { MultiselectGroup, UseMultiselectOptions } from './types';

export function useMultiselectValues({
  selectAllLabel,
  selectedAllByDefault,
  onChange,
  data,
  defaultValue = [],
}: UseMultiselectOptions) {
  const total = data.length;
  let initialState: string[] = [];

  if (defaultValue.length) {
    initialState = defaultValue.map((d) => d.text);
  } else if (selectedAllByDefault) {
    initialState = data.map((d) => d.text);
  }

  const [names, updateNames] = React.useState<string[]>(initialState);

  React.useEffect(() => {
    setNames(initialState);
  }, [data.length === 0]);

  function getRenderValue() {
    if (data.length === names.length && selectAllLabel) return selectAllLabel;
    else return `${names.length} items of ${total} of items`;
  }

  function getValue() {
    return names.map((n) => data.find((d) => d.text === n));
  }

  function setNames(newNames: string[]) {
    if (onChange !== undefined) {
      onChange(newNames.map((n) => data.find((d) => d.text === n)));
    }
    updateNames(newNames);
  }

  function selectAllToggle() {
    if (names.length === data.length) setNames([]);
    else setNames(data.map((d) => d.text));
  }

  const groups = data.reduce<MultiselectGroup[]>((acc, { grouping, text }) => {
    const item = acc.find((i) => i.grouping === grouping);
    if (item) item.data.push(text);
    else acc.push({ grouping, data: [text] });
    return acc;
  }, []);

  return { names, setNames, getValue, getRenderValue, groups, selectAllToggle };
}

