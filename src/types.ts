import { SelectProps } from '@mui/material';

export interface MultiselectListItem {
  id: number;
  grouping: string;
  text: string;
}

export interface UseMultiselectOptions {
  data: MultiselectListItem[];
  selectedAllByDefault?: boolean;
  selectAllLabel?: string;
  onChange?: (values: any) => void;
  defaultValue?: MultiselectListItem[];
}

export interface MultiselectAccordionProps extends SelectProps {
  name: string;
  label: string;
  data: MultiselectListItem[];
  selectAllLabel?: string;
  selectedAllByDefault?: boolean;
  onChange?: (values: any) => void;
  skeleton?: boolean;
  value?: MultiselectListItem[];
}

export interface MultiselectGroup {
  grouping: string;
  data: string[];
}

