import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import * as React from 'react';

interface Props {
  key: string;
  data: string[];
  setName: (names: string[]) => void;
  names: string[];
  territoryName: string;
}

export function SelectAccordionItem({
  key,
  data,
  names,
  setName,
  territoryName,
}: Props) {
  const handleClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();
    event.stopPropagation();
    if (allIncluded) {
      setName(names.filter((n) => !data.includes(n)));
      return;
    }

    setName([...names.filter((n) => !data.includes(n)), ...data]);
  };

  const handleItemClick =
    (name: string) => (e: React.MouseEvent<HTMLLIElement>) => {
      e.preventDefault();
      e.stopPropagation();
      if (names.indexOf(name) > -1) {
        setName(names.filter((n) => n !== name));
        return;
      }

      setName([...names, name]);
    };

  const indeterminate = data.some((d) => names.includes(d));
  const allIncluded = data.every((d) => names.includes(d));

  return (
    <Accordion key={key}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls={`${territoryName}-content`}
        id={`${territoryName}-header`}
      >
        <Typography sx={{ alignItems: 'center' }}>
          <Checkbox
            checked={allIncluded}
            onClick={handleClick}
            indeterminate={!allIncluded && indeterminate}
            inputProps={{ 'aria-label': 'controlled' }}
          />
          {territoryName}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        {data.map((name) => (
          <MenuItem key={name} value={name} onClick={handleItemClick(name)}>
            <Checkbox
              checked={names.indexOf(name) > -1}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            <ListItemText primary={name} />
          </MenuItem>
        ))}
      </AccordionDetails>
    </Accordion>
  );
}

